const request = require('request');
const { Client, logger } = require('camunda-external-task-client-js');
const BearerTokenInterceptor = require('camunda-external-task-bearer-token-interceptor');
const { Variables } = require("camunda-external-task-client-js");


// ################ Keycloak ##########################
let bearerTokenInterceptor = null;
if (process.env.KEYCLOAK_CLIENT_ID && process.env.KEYCLOAK_CLIENT_SECRET && process.env.KEYCLOAK_BASE_URL && process.env.KEYCLOAK_REALM_ID) {
  bearerTokenInterceptor = new BearerTokenInterceptor({
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    baseUrl: process.env.KEYCLOAK_BASE_URL,
    readlmId: process.env.KEYCLOAK_REALM_ID,
  });
}
// ################ Camunda ##########################
const config = { baseUrl: process.env.CAMUNDA_URL, use: logger, interceptors: bearerTokenInterceptor, workerId: 'workflow-messages', interval: 1000 };

// create a Client instance with custom configuration
const client = new Client(config);

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5hbHl0aWNzX3VzZXIifQ.Q8H_V6qh9w-mxHUY4_2IqmWNk_UdkbtacaOAsbP8peE'
const POSTGREST_TOKEN = process.env.POSTGREST_TOKEN?process.env.POSTGREST_TOKEN:token
const POSTGREST_URL = process.env.POSTGREST_URL

client.subscribe('get-all-users', async function({ task, taskService }) {

  request.get({
    'url': POSTGREST_URL+ '/persons',
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(body){
        let persons = JSON.parse(body);
        persons = persons.map(user => ({
          status: user.person_status,
          fullName: user.person_fullname,
          birthDate: user.person_birthdate,
          address: user.person_address,
          iin: user.iin,
          idCard: user.id_card,
          position: user.position,
          enterDate: user.enter_date,
          company: user.company,
          project: user.project,
          qualityCard: user.quality_card,
          insurance: user.insurance,
          comment: user.comment,
        }))
        const variables = new Variables().setAllTyped({
          persons: {
            value: persons,
            type: 'Json',
          },
        })
        taskService.complete(task, variables).then((result) => {
          console.log(`Result: ${result}`)
        })
      }
    }
  })
});
  // Запись в 
  client.subscribe('set-data-user', async function({ task, taskService }) {

    const typedValues = task.variables.getAllTyped();
    var partner = typedValues.newPerson.value
    var method = typedValues.card.value
    var headId = typedValues.initiator.value
    if (partner){
      if (method == 'change'){
        request({
          method: 'POST',
          uri: POSTGREST_URL+'/persons',
          auth: {
            'bearer': POSTGREST_TOKEN
          },
          'content-type': 'application/json',
          body: JSON.stringify({
            person_status: partner.status,
            person_fullname: partner.fullName,
            person_birthdate: partner.birthDate,
            person_address: partner.address,
            iin: partner.iin,
            id_card: partner.idCard,
            position: partner.position,
            enter_date: partner.enterDate,
            company: partner.company,
            project: partner.project,
            quality_card: partner.qualityCard,
            insurance: partner.insurance,
            head_id: headId,
          })
        }, function(error, response, body){
          if(error){
            console.log('error:=' + error);
          } else {
            console.log(body)
          }
        })
      } else if (method == 'read') {
        request({
          method: 'PATCH',
          uri: host+':3000/persons?iin=eq.'+partner.iin,
          auth: {
            'bearer': token
          },
          'content-type': 'application/json',
          body: JSON.stringify({
            person_status: 'unchecked',
            insurance: partner.insurance,
            quality_card: partner.qualityCard,
          })
        }, function(error, response, body){
          if(error){
            console.log('error:=' + error);
          } else {
            console.log(body)
          }
        })
      }
    } 
   
    await taskService.complete(task);
  });
  // взять новых для БиОТ
client.subscribe('get-unchecked-users', async function({ task, taskService }) {

  request.get({
    'url': POSTGREST_URL+ '/persons?person_status=eq.unchecked',
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(body){
        let persons = JSON.parse(body);
        persons = persons.map(user => ({
          status: user.person_status,
          fullName: user.person_fullname,
          birthDate: user.person_birthdate,
          address: user.person_address,
          iin: user.iin,
          idCard: user.id_card,
          position: user.position,
          enterDate: user.enter_date,
          company: user.company,
          project: user.project,
          qualityCard: user.quality_card,
          insurance: user.insurance,
        }))
        const variables = new Variables().setAllTyped({
          partners: {
            value: persons,
            type: 'Json',
          },
        })
        taskService.complete(task, variables).then((result) => {
          console.log(`Result: ${result}`)
        })
      }
    }
  })
});

client.subscribe('change-status', async function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  var status = typedValues.setStatus.value;
  var partners = typedValues.partners.value;
  var newStatus = '';
  if (partners){
    partners.forEach(partner => 
    {
      newStatus = status === 'processing' ? 'processing' : partner.status;
      request({
        method: 'PATCH',
        uri: POSTGREST_URL+ '/persons?iin=eq.'+partner.iin,
        auth: {
          'bearer': POSTGREST_TOKEN
        },
        'content-type': 'application/json',
        body: JSON.stringify({
          person_status: newStatus,
          comment: partner.comment
        })
      }, function(error, response, body){
        if(error){
          console.log('error:=' + error);
        } else {
          console.log(body)
        }
      })
    })
  } 
  await taskService.complete(task);
});

client.subscribe('get-checked-users', async function({ task, taskService }) {

  const searchStatus = task.variables.getAllTyped().searchStatus.value
  request.get({
    'url': POSTGREST_URL+ '/persons?person_status=eq.'+searchStatus,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(body){
        let persons = JSON.parse(body);
        persons = persons.map(user => ({
          status: user.person_status,
          fullName: user.person_fullname,
          birthDate: user.person_birthdate,
          address: user.person_address,
          iin: user.iin,
          idCard: user.id_card,
          position: user.position,
          enterDate: user.enter_date,
          company: user.company,
          project: user.project,
          qualityCard: user.quality_card,
          insurance: user.insurance,
          id: user.head_id,
        }))
          // Вытащить руководителей
        var headers = []
        persons.forEach(element=>{
            headers.push(element.id)
        })
          // Get unique headers
        var obj = {};
        for (var i = 0; i < headers.length; i++) {
          var str = headers[i];
          obj[str] = true; 
        }
        headers = Object.keys(obj) // array unique HeadId

        var partnersHeaders = [];
        for(var i=0; i < headers.length; i++) {
            var arr = []
            persons.forEach(element => {
                if (headers[i] === element.id) {
                    arr.push(element)
                }
            });
            partnersHeaders.push({headId: headers[i], partners: arr}) // new array partners grouped by
        }
        
        const variables = new Variables().setAllTyped({
          partnersHeaders: {
            value: partnersHeaders,
            type: 'Json',
          },
          checkedPartners: {
            value: persons,
            type: 'Json',
          },
        })
        taskService.complete(task, variables).then((result) => {
          console.log(`Result: ${result}`)
        })
      }
    }
  })
});