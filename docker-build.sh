#/bin/bash

TYPE=$1
if [ -z "$TYPE" ]; then
	TYPE="dev"
fi
VERSION=$2
if [ -z "$VERSION" ]; then
	VERSION="1.0.0"
fi

BASEDIR=`pwd`

case $TYPE in
    prod)
        cd "$BASEDIR"
        echo "Building production image"
        docker build -t registry.gitlab.com/erboltokpaev/bp-teams:$VERSION-prod .
        echo "Pushing production image to repository"
        docker push registry.gitlab.com/erboltokpaev/bp-teams:$VERSION-prod
        ;;
    dev)
        cd "$BASEDIR"
        echo "Building test image"
        docker build -t registry.gitlab.com/erboltokpaev/bp-teams:latest .
        echo "Pushing test image to repository"
        docker push registry.gitlab.com/erboltokpaev/bp-teams:latest
        ;;
    *)          
esac
