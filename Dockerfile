FROM node:8.11

ENV CAMUNDA_URL = 'http://localhost:8080/engine-rest'

WORKDIR /app

# libs
COPY package.json /app
COPY package-lock.json /app
RUN npm install

# main file
COPY worker.js /app

CMD node worker.js
